package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public JPAEmployeeRepository getJpaEmployeeRepository() {
        return jpaEmployeeRepository;
    }

    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll();
    }

    public EmployeeResponse findById(Long id) {
        if (jpaEmployeeRepository.findById(id).isEmpty())
            throw new EmployeeNotFoundException();
        else return EmployeeMapper.toResponse(jpaEmployeeRepository.findById(id).get());
    }

    public void update(Long id, Employee employee) {
        EmployeeResponse toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        //jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return getJpaEmployeeRepository().findAll()
                .stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee savedEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }

    public List<Employee> findByCompanyId(Long companyId) {
        return jpaEmployeeRepository.findAll().
                stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

}
