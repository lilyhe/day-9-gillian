package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }


    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return jpaCompanyRepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public Company findById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public void update(Long id, Company company) {
        Company toBeUpdatedCompany = findById(id);
        toBeUpdatedCompany.setName(company.getName());
        jpaCompanyRepository.save(toBeUpdatedCompany);
    }

    public Company create(Company company) {
        return jpaCompanyRepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return findById(id).getEmployees();
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
