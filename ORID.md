# day9-Gillian

## Objective:
Today we focused on JPA (Java Persistence API). Its role is to form a mapping between the relational database and the object, so that we do not need to deal with complex SQL statements in the specific operation of the database, as long as it is like the usual operation of the object.

## Reflective:
Although I have not learned JPA, I can use it quickly.

## Interpretive:

JPA encapsulates many of the underlying principles, so it is easy to use; Because today's exercise is based on yesterday's homework, JPA is easier to understand. It also gave me a better understanding of the three-layer architecture.

## Decisional:
There are a lot of annotations in JPA that I don't know about and need to explore more. Communicate more with classmates. We can find some interesting technical problems and ideas.










